import LoginPage from "./client src/LoginPage";
import PrivateRoute from "./client src/PrivateRoute";

function App() {
  return (
    <div className="App">
      <LoginPage />
      <PrivateRoute />
    </div>
  );
}

export default App;
